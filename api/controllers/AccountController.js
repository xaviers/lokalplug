var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment');

module.exports = {
    renderLatestActivitiesView: async(function (req, res) {
        moment.locale(req.getLocale());

        var latestBookmarkedMixtape = await(_.map(await(mixtapeService.getBookmarkedIdsByUserId(req.user.id, 5)), mixtapeService.getById));

        var locals = {
            pageTitle: req.__('accountPage.title') + ' - Lokal Plug',
            section: 'other',
            accountNav: 'latestActivities',
            moment: moment,
            latestBookmarked: latestBookmarkedMixtape,
            latestUploaded: await(mixtapeService.getUploadedMixtapeByUserId(req.user.id, 5))
        };

        return res.view('account', locals);
    }),

    renderMixtapesBookmarkedList: async(function (req, res) {
        moment.locale(req.getLocale());

        var title = req.__('accountPage.mixtapesLists.bookmarkedTitle');

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'latestBookmarked',
            title: title,
            moment: moment,
            mixtapes: await(_.map(await(mixtapeService.getBookmarkedIdsByUserId(req.user.id)), mixtapeService.getById))
        };

        return res.view('accountMixtapeList', locals);
    }),

    renderMixtapesUploadedList: async(function (req, res) {
        moment.locale(req.getLocale());

        var title = req.__('accountPage.mixtapesLists.uploadedTitle');

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'latestUploaded',
            title: title,
            moment: moment,
            mixtapes: await(mixtapeService.getUploadedMixtapeByUserId(req.user.id))
        };

        return res.view('accountMixtapeList', locals);
    }),

    renderSettingsView: function (req, res) {
        var title = req.__('accountPage.settings.title');

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'settings'
        };

        return res.view('accountSettings', locals);
    }
};
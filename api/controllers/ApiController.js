var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    getTrendingMixtapes: async(function (req, res) {
        var limit = 6;

        if (!_.isEmpty(req.query.limit))
            limit = _.parseInt(req.query.limit);

        var trendingMixtapesIds = await(mixtapeService.getTrendingIds(limit));

        var mixtapes = [];

        const retrieveMixtapes = async(function () {
            await(asyncService.asyncForEach(trendingMixtapesIds, async(function(id) {
                mixtapes.push(await(Mixtape.findOne({id: id, isConfirmed: true})
                    .populate('artists')
                    .populate('featuringArtists')
                    .populate('beatmakers')
                    .populate('featuringBeatmakers')
                    .populate('musicGroups')
                    .populate('featuringMusicGroups')
                    .populate('musicGenres')
                    .populate('uploader')
                    .populate('types')
                    .populate('tracks')
                    .populate('bookmarkedBy')
                    .populate('likedBy')
                    .populate('dislikedBy')
                    .populate('downloads')));
            })));

            return res.ok(_.map(mixtapes, apiMapperService.toMixtapeObject));
        });

        retrieveMixtapes();
    }),

    getLatestOfficialMixtapes: async(function (req, res) {
        var limit = 6;

        if (!_.isEmpty(req.query.limit))
            limit = _.parseInt(req.query.limit);

        var mixtapes = await(Mixtape.find({isOfficial: true, isConfirmed: true})
            .populate('artists')
            .populate('featuringArtists')
            .populate('beatmakers')
            .populate('featuringBeatmakers')
            .populate('musicGroups')
            .populate('featuringMusicGroups')
            .populate('musicGenres')
            .populate('uploader')
            .populate('types')
            .populate('tracks')
            .populate('bookmarkedBy')
            .populate('likedBy')
            .populate('dislikedBy')
            .populate('downloads')
            .limit(limit)
            .sort('releaseDate DESC'));

        return res.ok(_.map(mixtapes, apiMapperService.toMixtapeObject));
    }),

    getLatestUploadedMixtapes: async(function (req, res) {
        var limit = 6;

        if (!_.isEmpty(req.query.limit))
            limit = _.parseInt(req.query.limit);

        var mixtapes = await(Mixtape.find({isConfirmed: true})
            .populate('artists')
            .populate('featuringArtists')
            .populate('beatmakers')
            .populate('featuringBeatmakers')
            .populate('musicGroups')
            .populate('featuringMusicGroups')
            .populate('musicGenres')
            .populate('uploader')
            .populate('types')
            .populate('tracks')
            .populate('bookmarkedBy')
            .populate('likedBy')
            .populate('dislikedBy')
            .populate('downloads')
            .limit(limit)
            .sort('releaseDate DESC'));

        return res.ok(_.map(mixtapes, apiMapperService.toMixtapeObject));
    }),

    getMixtape: async(function (req, res) {
        if (_.isEmpty(req.param('slug')))
            return res.notFound();

        var mixtape = await(Mixtape.findOne({slug: req.param('slug'), isConfirmed: true})
            .populate('artists')
            .populate('featuringArtists')
            .populate('beatmakers')
            .populate('featuringBeatmakers')
            .populate('musicGroups')
            .populate('featuringMusicGroups')
            .populate('musicGenres')
            .populate('uploader')
            .populate('types')
            .populate('tracks')
            .populate('bookmarkedBy')
            .populate('likedBy')
            .populate('dislikedBy')
            .populate('downloads'));

        if (_.isEmpty(mixtape))
            return res.notFound();

        return res.ok(apiMapperService.toMixtapeObject(mixtape));
    }),

    getTracks: async(function (req, res) {
        if (_.isEmpty(req.query.slug))
            return res.notFound();

        var tracks = await(Track.find({slug: req.query.slug, isConfirmed: true})
            .populate('artist')
            .populate('featuringArtists')
            .populate('musicGroup')
            .populate('featuringMusicGroups')
            .populate('beatmakers')
            .populate('mixtape'));

        if (_.isEmpty(tracks))
            return res.notFound();

        return res.ok(_.map(tracks, apiMapperService.toTrackObject));
    })
};
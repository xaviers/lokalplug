var passport = require('passport'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    Promise = require('bluebird'),
    bhttp = Promise.promisifyAll(require('bhttp'));

module.exports = {

    _config: {
        actions: false,
        shortcuts: false,
        rest: false
    },

    renderLoginView: function(req, res) {
        var locals = {
            pageTitle: req.__('loginPage.title') + ' - Lokal Plug',
            section: 'other'
        };

        return res.view('login', locals);
    },

    login: async (function(req, res) {
        var recaptchaConfirmed = await(bhttp.post('https://www.google.com/recaptcha/api/siteverify', {
            secret: sails.config.recaptchaSecret,
            response: req.body['g-recaptcha-response'],
            remoteip: req.ip
        })).body.success;

        if (!recaptchaConfirmed) {
            return res.status(400).send({
                loginStatus: 'failed'
            });
        }

        passport.authenticate('local', function(err, user) {
            if ((err) || (!user)) {
                return res.status(400).send({
                    loginStatus: 'failed'
                });
            }
            req.logIn(user, function(err) {
                if (err) res.send(err);
                return res.status(200).send({
                    loginStatus: 'success'
                });
            });

        })(req, res);
    }),

    logout: function(req, res) {
        req.logout();
        res.redirect('/');
    }
};
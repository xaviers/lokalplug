var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment');

module.exports = {
    renderView: async(function(req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));
        var trendingMixtapesIds = await(mixtapeService.getTrendingIds(6));

        var locals = {
            pageTitle: 'Lokal Plug - ' + req.__('siteDescription'),
            section: 'home',
            moment: moment,
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            trendingMixtapes: [],
            latestOfficialMixtapes: await(Mixtape.find({isOfficial: true, isConfirmed: true})
                .populate('artists')
                .populate('beatmakers')
                .populate('musicGroups')
                .populate('musicGenres')
                .limit(6)
                .sort('releaseDate DESC')),
            latestUploadedMixtapes: await(Mixtape.find({isConfirmed: true})
                .populate('artists')
                .populate('beatmakers')
                .populate('musicGroups')
                .populate('musicGenres')
                .limit(6)
                .sort('releaseDate DESC'))
        };

        const retrieveMixtapes = async(function () {
            await(asyncService.asyncForEach(trendingMixtapesIds, async(function(id) {
                locals.trendingMixtapes.push(await(Mixtape.findOne({id: id, isConfirmed: true})
                    .populate('artists')
                    .populate('beatmakers')
                    .populate('musicGroups')
                    .populate('musicGenres')));
            })));

            return res.view('homepage', locals);
        });

        retrieveMixtapes();
    })
};
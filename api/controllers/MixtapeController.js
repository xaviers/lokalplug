var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment'),
    URI = require('urijs');

module.exports = {
    renderAllMixtapesView: async(function (req, res) {
        moment.locale(req.getLocale());

        var today = moment().hour(0).minute(0).second(0).millisecond(0);
        var thisWeek = moment(today).subtract(1, 'days').startOf('week');
        var lastTwoWeeks = moment(thisWeek).subtract(16, 'days');
        var thisMonth = moment(today).subtract(1, 'days').startOf('month');
        var thisYear = moment(today).subtract(1, 'days').startOf('year');

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var locals = {
            pageTitle: req.__('mixtapesPage.title') + ' - Lokal Plug',
            section: 'mixtape',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment
        };

        locals.sortBy = {
            sortFilters: {
                default: {
                    label: 'latest',
                    query: '',
                    value: 'releaseDate DESC'
                },
                mostrated: {
                    label: 'mostRated',
                    query: 'mostrated',
                    value: 'rate DESC'
                },
                mostdownloaded: {
                    label: 'mostDownloaded',
                    query: 'mostdownloaded',
                    value: 'downloadCount DESC'
                }
            }
        };

        var sortBy = locals.sortBy.sortFilters.default.value;
        locals.sortBy.active = locals.sortBy.sortFilters.default;

        if (!_.isEmpty(req.query.sort) && locals.sortBy.sortFilters[req.query.sort]) {
            sortBy = locals.sortBy.sortFilters[req.query.sort].value;
            locals.sortBy.active = locals.sortBy.sortFilters[req.query.sort];
            _.unset(locals.sortBy, "sortFilters." + req.query.sort);
        }
        else {
            _.unset(locals.sortBy, "sortFilters.default")
        }

        locals.filters = {
            todayDate: moment().format('YYYY-MM-DD'),
            labels: {
                isOfficial: [
                    {
                        label: 'onlyOfficials',
                        query: 'true',
                        value: true
                    },
                    {
                        label: 'onlyNonOfficials',
                        query: 'false',
                        value: false
                    }
                ]
            },
            dateFilter: {
                releaseDate: [
                    {
                        label: 'anyTime',
                        query: '',
                        value: '*'
                    },
                    {
                        label: 'today',
                        query: 'today',
                        value: today.format('YYYY-MM-DD')
                    },
                    {
                        label: 'thisWeek',
                        query: 'thisweek',
                        value: thisWeek.format('YYYY-MM-DD')
                    },
                    {
                        label: 'lastTwoWeeks',
                        query: 'lastweeks',
                        value: lastTwoWeeks.format('YYYY-MM-DD')
                    },
                    {
                        label: 'thisMonth',
                        query: 'thismonth',
                        value: thisMonth.format('YYYY-MM-DD')
                    },
                    {
                        label: 'thisYear',
                        query: 'thisyear',
                        value: thisYear.format('YYYY-MM-DD')
                    }
                ]
            }
        };

        var filters = queryService.filterListQuery(locals.filters, req.query);
        filters.isConfirmed = true;

        var data = {};

        data.totalPages = parseInt(await(Mixtape.count(filters)) / 24) + 1;
        data.currentPage = req.query.page || 1;
        data.previous = data.currentPage - 1 > 0;
        data.next = data.currentPage < data.totalPages;

        if (data.currentPage > data.totalPages) {
            var url = new URI(req.url).removeSearch('page').addSearch('page', data.totalPages);
            return res.redirect(url.toString());
        }

        data.results = await(Mixtape.find({where: filters, sort: sortBy}).paginate({page: data.currentPage, limit: 24})
            .populate('artists')
            .populate('beatmakers')
            .populate('musicGroups')
            .populate('musicGenres'));

        data.filters = {};

        if (req.query.isOfficial) {
            data.filters.isOfficial = req.query.isOfficial;
        }

        locals.data = data;

        var releaseDateFilters = _.map(locals.filters.dateFilter.releaseDate, _.clone);
        locals.filters.dateFilter['releaseDateActive'] = _.head(_.filter(_.reverse(releaseDateFilters), function (filter) {
            return filter.query === req.query['releaseDate'] || filter.query === '';
        }));

        _.remove(locals.filters.dateFilter.releaseDate, function (filter) {
            return locals.filters.dateFilter.releaseDateActive.label === filter.label;
        });

        return res.view('mixtapes', locals);
    }),

    renderMixtapeView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')})
            .populate('artists')
            .populate('musicGroups')
            .populate('beatmakers')
            .populate('featuringArtists')
            .populate('uploader'));

        mixtape.owners = _.concat(mixtape.artists, mixtape.musicGroups, mixtape.beatmakers);

        mixtape.tracks = await(Track.find({mixtape: mixtape.id})
            .populate('artist')
            .populate('featuringArtists')
            .populate('musicGroup')
            .populate('featuringMusicGroups')
            .populate('beatmakers'));

        _.forEach(mixtape.tracks, function (track, index) {
            var description = track.title;

            if (!_.isEmpty(track.artist) && _.indexOf(_.map(mixtape.owners, 'name'), track.artist.name) === -1)
                description += ' - ' + track.artist.name;
            else if (!_.isEmpty(track.musicGroup) && _.indexOf(mixtape.owners, track.musicGroup.name) === -1)
                description += ' - ' + track.musicGroup.name;

            var featurings = _.concat(track.featuringArtists, track.featuringMusicGroups);

            if (!_.isEmpty(featurings))
                description += ' Feat. ' + _.join(_.map(featurings, 'name'), ', ');

            if (!_.isEmpty(track.beatmakers))
                description += ' Prod. ' + _.join(_.map(track.beatmakers, 'name'), ', ');

            mixtape.tracks[index].description = description;
        });

        if (_.isUndefined(mixtape) || (!mixtape.isConfirmed && !(req.user && req.user.isAdmin)))
            return res.notFound();

        var artists = _.join(_.map(_.concat(mixtape.artists, mixtape.musicGroups, mixtape.beatmakers), 'name'), ', ');

        var locals = {
            artists: artists,
            pageTitle: mixtape.title + ' - ' + artists,
            section: 'mixtape',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment,
            currentUrl: 'https://lokalplug.com' + req.path,
            mixtape: mixtape,
            isLiked: false,
            isDisliked: false,
            isBookmarked: false
        };

        if (req.isAuthenticated()) {
            locals.isLiked = await(UserMixtapeLike.count({mixtape: locals.mixtape.id, user: req.user.id})) === 1;
            locals.isDisliked = await(UserMixtapeDislike.count({mixtape: locals.mixtape.id, user: req.user.id})) === 1;
            locals.isBookmarked = await(UserMixtapeBookmark.count({
                mixtape: locals.mixtape.id,
                user: req.user.id
            })) === 1;
        }

        locals.mixtape.featuringArtists = _.orderBy(locals.mixtape.featuringArtists, "name");

        locals.featuringArtistsPopover = '<ul class="list-inline featuring-artists">';
        _.forEach(locals.mixtape.featuringArtists, function (artist) {
            locals.featuringArtistsPopover += '<li><a href="/artist/' + artist.slug + '"><span class="avatar"><img src="' + artist.pictureUrl + '">' + artist.name + '</span></a></li>'
        });
        locals.featuringArtistsPopover += '</ul>';

        return res.view('mixtape', locals);
    }),

    renderAllMixtapesAdminView: function (req, res) {
        var title = 'Toutes les mixtapes';

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'allMixtapes'
        };

        return res.view('accountAllMixtapes', locals);
    },

    getAllMixtapes: async(function (req, res) {
        moment.locale('fr');

        Mixtape.find()
            .populate('artists')
            .populate('musicGroups')
            .populate('beatmakers')
            .populate('uploader')
            .skip(req.query.skip)
            .limit(req.query.limit)
            .sort(req.query.order_by + ' ' + req.query.sort)
            .exec(function (err, mixtapes) {
                _.forEach(mixtapes, function (mixtape) {
                    mixtape.createdAt = moment(mixtape.createdAt).format('LL');
                });

                if (err)
                    return res.serverError();

                return res.ok(mixtapes);
            })
    }),

    downloadMixtape: async(function (req, res) {
        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug'), isConfirmed: true}));

        if (_.isUndefined(mixtape) || _.isEmpty(mixtape.fileUrl))
            return res.notFound();

        var download = {
            mixtape: mixtape.id,
            date: moment().format('YYYY-MM-DD')
        };

        if (req.isAuthenticated())
            download.user = req.user.id;

        await(Download.create(download));

        await(Mixtape.update({id: mixtape.id}, {downloadCount: (mixtape.downloadCount + 1)}));

        return res.redirect(mixtape.fileUrl);
    }),

    bookmark: function (req, res) {
        if (!req.user) return res.badRequest();

        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')}));
        var isBookmarking = false;

        if (await(UserMixtapeBookmark.count({mixtape: mixtape.id, user: req.user.id})) === 0) {
            mixtape.bookmarkedBy.add(req.user.id);
            isBookmarking = true;
        } else {
            mixtape.bookmarkedBy.remove(req.user.id);
        }

        mixtape.save(async(function () {
            return res.ok({isBookmarking: isBookmarking});
        }));
    },

    like: async(function (req, res) {
        if (!req.user) return res.badRequest();

        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')}));
        var isLiking = false;

        if (await(UserMixtapeLike.count({mixtape: mixtape.id, user: req.user.id})) === 0) {
            mixtape.likedBy.add(req.user.id);
            mixtape.dislikedBy.remove(req.user.id);
            isLiking = true;
        } else {
            mixtape.likedBy.remove(req.user.id);
        }

        mixtape.save(async(function (err) {
            return res.ok({rate: await(mixtapeService.updateRate(mixtape.slug)), isLiking: isLiking});
        }));
    }),

    dislike: async(function (req, res) {
        if (!req.user) return res.badRequest();

        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')}));
        var isDisliking = false;

        if (await(UserMixtapeDislike.count({mixtape: mixtape.id, user: req.user.id})) === 0) {
            mixtape.dislikedBy.add(req.user.id);
            mixtape.likedBy.remove(req.user.id);
            isDisliking = true;
        } else {
            mixtape.dislikedBy.remove(req.user.id);
        }

        mixtape.save(async(function () {
            return res.ok({rate: await(mixtapeService.updateRate(mixtape.slug)), isDisliking: isDisliking});
        }));
    }),

    deleteMixtape: async(function (req, res) {
        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')})
            .populate('types')
            .populate('musicGroups')
            .populate('featuringMusicGroups')
            .populate('artists')
            .populate('featuringArtists')
            .populate('beatmakers')
            .populate('featuringBeatmakers')
            .populate('musicGenres')
            .populate('downloads')
            .populate('bookmarkedBy')
            .populate('likedBy')
            .populate('dislikedBy')
            .populate('tracks'));

        if (_.isEmpty(mixtape))
            return res.notFound();

        if (mixtape.uploader.id !== req.user.id && !req.user.isAdmin) {
            return res.badRequest();
        }

        mixtapeService.removeMixtapeRelations(mixtape);


        _.forEach(mixtape.downloads, function (download) {
            var user = await(User.findOne({id: download.user})
                .populate('downloads'));

            _.pullAt(user.downloads, _.indexOf(_.map(user.downloads, 'id'), download.id));
            await(user.save());

            await(Download.destroy({id: download.id}));
        });

        _.forEach(mixtape.tracks, function (track) {
            trackService.removeTrackRelations(track);
            await(Track.destroy({id: track.id}));
        });

        Mixtape.destroy({slug: req.param('mixtapeSlug')}).exec(function (err) {
            if (err) {
                return res.serverError();
            }

            return res.ok();
        });
    })
};
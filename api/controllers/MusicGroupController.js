var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment'),
    slugify = require('slug'),
    URI = require('urijs');

module.exports = {
    renderAllMusicGroupsView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var locals = {
            pageTitle: req.__('musicGroupsPage.title') + ' - Lokal Plug',
            section: 'music-group',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment
        };

        var data = {};

        data.totalPages = parseInt(await(MusicGroup.count()) / 24) + 1;
        data.currentPage = req.query.page || 1;
        data.previous = data.currentPage - 1 > 0;
        data.next = data.currentPage < data.totalPages;

        if (data.currentPage > data.totalPages) {
            var url = new URI(req.url).removeSearch('page').addSearch('page', data.totalPages);
            return res.redirect(url.toString());
        }

        data.results = await(MusicGroup.find({lastOfficialActivityAt: {'!': null}})
            .sort('lastOfficialActivityAt DESC')
            .paginate({page: data.currentPage, limit: 24})
            .populate('ownMixtapes'));

        _.forEach(data.results, function (musicGroup, index) {
            var rate = 0;

            _.forEach(musicGroup.ownMixtapes, function (mixtape) {
                rate = rate + mixtape.rate;
            });

            musicGroup.rate = rate;

            data.results[index] = musicGroup;
        });

        data.results = _.sortBy(data.results, [function (musicGroup) {
            return moment(musicGroup.lastOfficialActivityAt).valueOf();
        }, "rate"]).reverse();

        locals.data = data;

        return res.view('musicGroups', locals);
    }),

    renderMusicGroupView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var musicGroup = await(MusicGroup.findOne({slug: req.param('musicGroupSlug')}));

        if (_.isUndefined(musicGroup))
            return res.notFound();

        var section = 'music-group';

        var locals = {
            pageTitle: musicGroup.name + ' - Lokal Plug',
            section: section,
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment,
            artist: musicGroup,
            ownMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByOwner(musicGroup.slug, 'musicGroup')),
                isConfirmed: true
            })
                .populate('artists')
                .populate('beatmakers')
                .populate('musicGroups')
                .populate('musicGenres')),
            featuringMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByFeaturing(musicGroup.slug, 'musicGroup')),
                isConfirmed: true
            })
                .populate('artists')
                .populate('beatmakers')
                .populate('musicGroups')
                .populate('musicGenres'))
        };

        return res.view('artist', locals);
    }),

    renderAllMusicGroupsAdminView: function (req, res) {
        var title = 'Tous les groupes';

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'allMusicGroups'
        };

        return res.view('accountAllMusicGroups', locals);
    },

    getAllMusicGroups: function (req, res) {
        moment.locale('fr');

        MusicGroup.find()
            .populate('artists')
            .populate('beatmakers')
            .populate('owners')
            .skip(req.query.skip)
            .limit(req.query.limit)
            .sort(req.query.order_by + ' ' + req.query.sort)
            .exec(function (err, musicGroups) {
                _.forEach(musicGroups, function (musicGroup) {
                    musicGroup.lastOfficialActivityAt = moment(musicGroup.lastOfficialActivityAt).format('LL');
                    musicGroup.createdAt = moment(musicGroup.createdAt).format('LL');
                });

                if (err)
                    return res.serverError();

                return res.ok(musicGroups);
            });
    },

    createMusicGroup: function (req, res) {
        req.body.slug = slugify(req.body.name, {lower: true});

        var artistIds = req.body.artists;
        _.unset(req.body, 'artists');

        var musicGroup = await(MusicGroup.create(req.body));
        var artists = await(Artist.find({id: artistIds}));
        var beatmakers = await(Beatmaker.find({id: artistIds}));

        _.forEach(artists, function (artist) {
            artist.musicGroups.add(musicGroup.id);
            artist.save(function (err) {
            });
        });

        _.forEach(beatmakers, function (beatmaker) {
            beatmaker.musicGroups.add(musicGroup.id);
            beatmaker.save(function (err) {
            });
        });

        return res.ok();
    },

    deleteMusicGroup: async(function (req, res) {
        var musicGroup = await(MusicGroup.findOne({slug: req.param('musicGroupSlug')})
            .populate('artists')
            .populate('beatmakers')
            .populate('ownTracks')
            .populate('featuringTracks')
            .populate('ownMixtapes')
            .populate('featuringMixtapes')
            .populate('owners'));

        if (_.isEmpty(musicGroup))
            return res.notFound();

        if (!_.map(musicGroup.owners, 'id').includes(req.user.id) && !req.user.isAdmin)
            return res.badRequest();

        if (!_.isEmpty(musicGroup.ownMixtapes) || !_.isEmpty(musicGroup.featuringMixtapes)
            || !_.isEmpty(musicGroup.ownTracks) || !_.isEmpty(musicGroup.featuringTracks)
            || !_.isEmpty(musicGroup.artists) || !_.isEmpty(musicGroup.beatmakers))
            return res.badRequest('music group with id \'' + musicGroup.id + '\' has at least one relation with a mixtape or contains members, please delete them first');

        musicGroupService.removeMusicGroupRelations(musicGroup);

        MusicGroup.destroy({slug: req.param('musicGroupSlug')}).exec(function (err) {
            if (err)
                return res.serverError();

            return res.ok();
        });
    })
};
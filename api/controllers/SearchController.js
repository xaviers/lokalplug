var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment');

module.exports = {
    search: async(function(req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var locals = {
            pageTitle: req.__('searchPage.title') + ' - Lokal Plug',
            section: 'other',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment,
            query: req.query.q
        };

        if (!_.isEmpty(req.query.q)) {
            locals.pageTitle = req.__('searchPage.resultsFor') + '\'' + req.query.q + '\' - Lokal Plug';
            locals.mixtapes = await(searchService.searchMixtapes(req.query.q));
            locals.artists = await(searchService.searchArtists(req.query.q));
            locals.djs = await(searchService.searchDJs(req.query.q));
            locals.beatmakers = await(searchService.searchBeatmakers(req.query.q));
            locals.musicGroups = await(searchService.searchMusicGroups(req.query.q));
        }

        return res.view('search', locals);
    })
};
var mod_spawnasync = require('spawn-async'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    cp = require('cp');

module.exports = {
    generate: async(function(req, res) {
        if (!await(apiService.checkAdminKey(req.query.key)))
            return res.forbidden();

        var worker = mod_spawnasync.createWorker({'log': req._sails.log});

        var url = sails.config.environment !== 'production' ? 'http://localhost:3000' : 'https://lokalplug.com';

        var argv = ['osg', './assets/sitemap.xml', url];

        worker.aspawn(argv, function (err) {
            if (err)
                return res.serverError();

            cp('./assets/sitemap.xml', './.tmp/public/sitemap.xml', function (err) {
                if (err)
                    return res.serverError();

                return res.ok();
            });
        });
    })
};
module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        type: {
            type: 'string',
            enum: ['artist', 'dj'],
            required: true
        },
        pictureUrl: {
            type: 'text'
        },
        coverUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultArtistCoverUrl
        },
        lastOfficialActivityAt: {
            type: 'date'
        },
        officialMixtapesCount: {
            type: 'integer',
            defaultsTo: 0
        },
        officialTracksCount: {
            type: 'integer',
            defaultsTo: 0
        },
        ownTracks: {
            collection: 'track',
            via: 'artist'
        },
        featuringTracks: {
            collection: 'track',
            via: 'featuringArtists',
            dominant: true
        },
        ownMixtapes: {
            collection: 'mixtape',
            via: 'artists',
            dominant: true
        },
        featuringMixtapes: {
            collection: 'mixtape',
            via: 'featuringArtists',
            dominant: true
        },
        musicGroups: {
            collection: 'musicGroup',
            via: 'artists'
        },
        owners: {
            collection: 'user',
            via: 'artists'
        }
    },
    beforeCreate: function (artist, cb) {
        if (artist.type === 'artist')
            artist.pictureUrl = sails.config.properties.defaultArtistPictureUrl;
        else
            artist.pictureUrl = sails.config.properties.defaultDJPictureUrl;

        cb();
    }
};
module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        pictureUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultMusicGroupPictureUrl
        },
        coverUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultArtistCoverUrl
        },
        lastOfficialActivityAt: {
            type: 'date'
        },
        officialMixtapesCount: {
            type: 'integer',
            defaultsTo: 0
        },
        officialTracksCount: {
            type: 'integer',
            defaultsTo: 0
        },
        artists: {
            collection: 'artist',
            via: 'musicGroups',
            dominant: true
        },
        beatmakers: {
            collection: 'beatmaker',
            via: 'musicGroups',
            dominant: true
        },
        ownTracks: {
            collection: 'track',
            via: 'musicGroup'
        },
        featuringTracks: {
            collection: 'track',
            via: 'featuringMusicGroups',
            dominant: true
        },
        ownMixtapes: {
            collection: 'mixtape',
            via: 'musicGroups',
            dominant: true
        },
        featuringMixtapes: {
            collection: 'mixtape',
            via: 'featuringMusicGroups',
            dominant: true
        },
        owners: {
            collection: 'user',
            via: 'musicGroups'
        }
    }
};
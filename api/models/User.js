var uuidv1 = require('uuid/v1'),
    bcrypt = require('bcrypt'),
    genId = require('gen-id')('XXXXXXXXXX');

module.exports = {
    attributes: {
        username: {
            type: 'string',
            required: true,
            unique: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        avatarUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultAvatarUrl
        },
        email: {
            type: 'email',
            required: true,
            unique: true
        },
        newEmail: {
            type: 'email',
            unique: true
        },
        emailConfirmationId: {
            type: 'text'
        },
        password: {
            type: 'string',
            minLength: 6,
            required: true
        },
        artists: {
            collection: 'artist',
            via: 'owners',
            dominant: true
        },
        beatmakers: {
            collection: 'beatmaker',
            via: 'owners',
            dominant: true
        },
        musicGroups: {
            collection: 'musicGroup',
            via: 'owners',
            dominant: true
        },
        mixtapesBookmarked: {
            collection: 'mixtape',
            via: 'user',
            through: 'usermixtapebookmark'
        },
        mixtapesLiked: {
            collection: 'mixtape',
            via: 'user',
            through: 'usermixtapelike'
        },
        mixtapesDisliked: {
            collection: 'mixtape',
            via: 'user',
            through: 'usermixtapedislike'
        },
        mixtapesUploaded: {
            collection: 'mixtape',
            via: 'uploader'
        },
        downloads: {
            collection: 'download',
            via: 'user'
        },
        active: {
            type: 'boolean',
            defaultsTo: false
        },
        activationId: {
            type: 'text',
            defaultsTo: function () {
                return uuidv1();
            }
        },
        isUploader: {
            type: 'boolean',
            defaultsTo: false
        },
        isAdmin: {
            type: 'boolean',
            defaultsTo: false
        },
        apiKey: {
            type: 'string',
            defaultsTo: function () {
                return genId.generate();
            }
        },
        toJSON: function () {
            var obj = this.toObject();
            delete obj.password;
            return obj;
        }
    },
    beforeCreate: function (user, cb) {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {
                    user.password = hash;
                    cb();
                }
            });
        });
    }
};
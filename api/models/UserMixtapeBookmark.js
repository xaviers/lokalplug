var moment = require('moment');

module.exports = {
    attributes: {
        user:{
            model: 'user'
        },
        mixtape: {
            model: 'mixtape'
        }
    }
};
module.exports = function(req, res, next) {
    if (req.isAuthenticated() && req.user.isAdmin === true) {
        return next();
    }
    else{
        return res.notFound();
    }
};
module.exports = {
    toMixtapeObject: function (mixtape) {
        if (_.isUndefined(mixtape))
            return mixtape;

        return {
            title: mixtape.title,
            slug: mixtape.slug,
            types: _.isUndefined(mixtape.types) ? [] : _.map(mixtape.types, this.toMixtapeTypeObject),
            coverUrl: mixtape.coverUrl,
            musicGroups: _.isUndefined(mixtape.musicGroups) ? [] : _.map(mixtape.musicGroups, this.toMusicGroupObject),
            featuringMusicGroups: _.isUndefined(mixtape.featuringMusicGroups) ? [] : _.map(mixtape.featuringMusicGroups, this.toMusicGroupObject),
            artists: _.isUndefined(mixtape.artists) ? [] : _.map(mixtape.artists, this.toArtistObject),
            featuringArtists: _.isUndefined(mixtape.featuringArtists) ? [] : _.map(mixtape.featuringArtists, this.toArtistObject),
            beatmakers: _.isUndefined(mixtape.beatmakers) ? [] : _.map(mixtape.beatmakers, this.toBeatmakerObject),
            featuringBeatmakers: _.isUndefined(mixtape.featuringBeatmakers) ? [] : _.map(mixtape.featuringBeatmakers, this.toBeatmakerObject),
            musicGenres: _.isUndefined(mixtape.musicGenres) ? [] : _.map(mixtape.musicGenres, this.toMusicGenreObject),
            downloadCount: mixtape.downloadCount,
            downloads: _.isUndefined(mixtape.downloads) ? [] : _.map(mixtape.downloads, this.toDownloadObject),
            rate: mixtape.rate,
            releaseDate: mixtape.releaseDate,
            uploader: this.toUserObject(mixtape.uploader),
            bookmarkedBy: _.isUndefined(mixtape.bookmarkedBy) ? [] : _.map(mixtape.bookmarkedBy, this.toUserObject),
            likedBy: _.isUndefined(mixtape.likedBy) ? [] : _.map(mixtape.likedBy, this.toUserObject),
            dislikedBy: _.isUndefined(mixtape.dislikedBy) ? [] : _.map(mixtape.dislikedBy, this.toUserObject),
            tracks: _.isUndefined(mixtape.tracks) ? [] : _.map(mixtape.tracks, this.toTrackObject),
            isOfficial: mixtape.isOfficial,
            upcoming: mixtape.upcoming,
            isFileAvailable: mixtape.isFileAvailable,
            fileUrl: mixtape.fileUrl
        }
    },

    toUserObject: function (user) {
        if (_.isUndefined(user))
            return user;

        return {
            username: user.username,
            slug: user.slug,
            avatarUrl: user.avatarUrl,
            artists: _.isUndefined(user.artists) ? [] : _.map(user.artists, this.toArtistObject),
            beatmakers: _.isUndefined(user.beatmakers) ? [] : _.map(user.beatmakers, this.toBeatmakerObject),
            musicGroups: _.isUndefined(user.musicGroups) ? [] : _.map(user.musicGroups, this.toMusicGroupObject),
            mixtapesBookmarked: _.isUndefined(user.mixtapesBookmarked) ? [] : _.map(user.mixtapesBookmarked, this.toMixtapeObject),
            mixtapesLiked: _.isUndefined(user.mixtapesLiked) ? [] : _.map(user.mixtapesLiked, this.toMixtapeObject),
            mixtapesDisliked: _.isUndefined(user.mixtapesDisliked) ? [] : _.map(user.mixtapesDisliked, this.toMixtapeObject),
            mixtapesUploaded: _.isUndefined(user.mixtapesUploaded) ? [] : _.map(user.mixtapesUploaded, this.toMixtapeObject),
            downloads: _.isUndefined(user.downloads) ? [] : _.map(user.downloads, this.toDownloadObject),
            isAdmin: _.isUndefined(user.isAdmin) ? [] : user.isAdmin
        }
    },

    toTrackObject: function (track) {
        if (_.isUndefined(track))
            return track;

        return {
            title: track.title,
            slug: track.slug,
            fileUrl: track.fileUrl,
            mixtape: this.toMixtapeObject(track.mixtape),
            number: track.number,
            artist: this.toArtistObject(track.artist),
            featuringArtists: _.isUndefined(track.featuringArtists) ? [] : _.map(track.featuringArtists, this.toArtistObject),
            musicGroup: this.toMusicGroupObject(track.musicGroup),
            featuringMusicGroup: _.isUndefined(track.featuringMusicGroup) ? [] : _.map(track.featuringMusicGroup, this.toMusicGroupObject),
            beatmakers: _.isUndefined(track.beatmakers) ? [] : _.map(track.beatmakers, this.toBeatmakerObject),
            downloadCount: track.downloadCount,
            playCount: track.playCount,
            isOfficial: track.isOfficial
        }
    },

    toMusicGenreObject: function (musicGenre) {
        if (_.isUndefined(musicGenre))
            return musicGenre;

        return {
            name: musicGenre.name,
            slug: musicGenre.slug,
            color: musicGenre.color,
            mixtapes: _.isUndefined(musicGenre.mixtapes) ? [] : _.map(musicGenre.mixtapes, this.toMixtapeObject)
        }
    },

    toDownloadObject: function (download) {
        if (_.isUndefined(download))
            return download;

        return {
            mixtape: this.toMixtapeObject(download.mixtape),
            date: download.date,
            user: this.toUserObject(download.user)
        }
    },

    toMixtapeTypeObject: function (mixtapeType) {
        if (_.isUndefined(mixtapeType))
            return mixtapeType;

        return {
            name: mixtapeType.name,
            slug: mixtapeType.slug,
            mixtapes: _.isUndefined(mixtapeType.mixtapes) ? [] : this.toMixtapeObject(mixtapeType.mixtapes)
        }
    },

    toArtistObject: function (artist) {
        if (_.isUndefined(artist))
            return artist;

        return {
            name: artist.name,
            slug: artist.slug,
            type: artist.type === 'artist' ? 'Artist' : 'DJ',
            pictureUrl: artist.pictureUrl,
            coverUrl: artist.coverUrl,
            lastOfficialActivityAt: artist.lastOfficialActivityAt,
            officialTracksCount: artist.officialTracksCount,
            officialMixtapesCount: artist.officialMixtapesCount,
            ownTracks: _.isUndefined(artist.ownTracks) ? [] : _.map(artist.ownTracks, this.toTrackObject),
            featuringTracks: _.isUndefined(artist.featuringTracks) ? [] : _.map(artist.featuringTracks, this.toTrackObject),
            ownMixtapes: _.isUndefined(artist.ownMixtapes) ? [] : _.map(artist.ownMixtapes, this.toMixtapeObject),
            featuringMixtapes: _.isUndefined(artist.featuringMixtapes) ? [] : _.map(artist.featuringMixtapes, this.toMixtapeObject),
            musicGroups: _.isUndefined(artist.musicGroups) ? [] : _.map(artist.musicGroups, this.toMusicGroupObject),
            owners: _.isUndefined(artist.owners) ? [] : _.map(artist.owners, this.toUserObject)
        }
    },

    toBeatmakerObject: function (beatmaker) {
        if (_.isUndefined(beatmaker))
            return beatmaker;

        return {
            name: beatmaker.name,
            slug: beatmaker.slug,
            pictureUrl: beatmaker.pictureUrl,
            coverUrl: beatmaker.coverUrl,
            lastOfficialActivityAt: beatmaker.lastOfficialActivityAt,
            officialTracksCount: beatmaker.officialTracksCount,
            officialMixtapesCount: beatmaker.officialMixtapesCount,
            ownTracks: _.isUndefined(beatmaker.ownTracks) ? [] : _.map(beatmaker.ownTracks, this.toTrackObject),
            featuringTracks: _.isUndefined(beatmaker.featuringTracks) ? [] : _.map(beatmaker.featuringTracks, this.toTrackObject),
            ownMixtapes: _.isUndefined(beatmaker.ownMixtapes) ? [] : _.map(beatmaker.ownMixtapes, this.toMixtapeObject),
            featuringMixtapes: _.isUndefined(beatmaker.featuringMixtapes) ? [] : _.map(beatmaker.featuringMixtapes, this.toMixtapeObject),
            musicGroups: _.isUndefined(beatmaker.musicGroups) ? [] : _.map(beatmaker.musicGroups, this.toMusicGroupObject),
            owners: _.isUndefined(beatmaker.owners) ? [] : _.map(beatmaker.owners, this.toUserObject)
        }
    },

    toMusicGroupObject: function (musicGroup) {
        if (_.isUndefined(musicGroup))
            return musicGroup;

        return {
            name: musicGroup.name,
            slug: musicGroup.slug,
            pictureUrl: musicGroup.pictureUrl,
            coverUrl: musicGroup.coverUrl,
            lastOfficialActivityAt: musicGroup.lastOfficialActivityAt,
            officialTracksCount: musicGroup.officialTracksCount,
            officialMixtapesCount: musicGroup.officialMixtapesCount,
            artists: _.isUndefined(musicGroup.artists) ? [] : _.map(musicGroup.artists, this.toArtistObject),
            beatmakers: _.isUndefined(musicGroup.beatmakers) ? [] : _.map(musicGroup.beatmakers, this.toBeatmakerObject),
            ownTracks: _.isUndefined(musicGroup.ownTracks) ? [] : _.map(musicGroup.ownTracks, this.toTrackObject),
            featuringTracks: _.isUndefined(musicGroup.featuringTracks) ? [] : _.map(musicGroup.featuringTracks, this.toTrackObject),
            ownMixtapes: _.isUndefined(musicGroup.ownMixtapes) ? [] : _.map(musicGroup.ownMixtapes, this.toMixtapeObject),
            featuringMixtapes: _.isUndefined(musicGroup.featuringMixtapes) ? [] : _.map(musicGroup.featuringMixtapes, this.toMixtapeObject),
            owners: _.isUndefined(musicGroup.owners) ? [] : _.map(musicGroup.owners, this.toUserObject)
        }
    }
};
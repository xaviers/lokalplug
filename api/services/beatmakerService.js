var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    removeBeatmakerRelations: async(function (beatmaker) {
        _.forEach(beatmaker.musicGroups, function (musicGroup) {
            musicGroup.beatmakers.remove(beatmaker.id);
            await(musicGroup.save());
        });

        _.forEach(beatmaker.owners, function (owner) {
            owner.beatmakers.remove(beatmaker.id);
            await(owner.save());
        });
    })
};
var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    emailValidator = require('email-validator'),
    slugify = require('slug');

module.exports = {
    checkSignupFields: async(function (form) {
        if (_.isEmpty(form.username) || _.isEmpty(form.email) || _.isEmpty(form.password) || _.isEmpty(form.passwordConfirmation)) {
            return false;
        }

        var usersByUsernameCount = await(User.count({slug: slugify(form.username, {lower: true})}));
        var usersByEmailCount = await(User.count({email: form.email}));

        if (usersByUsernameCount > 0 || usersByEmailCount > 0) {
            return false;
        }

        if (form.username.length < 4 || form.username.length > 20 || form.password.length < 5) {
            return false;
        }

        if (!/^[a-zA-Z0-9]+$/.test(form.username)) {
            return false;
        }

        if (!emailValidator.validate(form.email)) {
            return false;
        }

        if (form.password !== form.passwordConfirmation) {
            return false;
        }

        return true;
    }),

    checkBasicSettingsFormFields: async(function (form, user) {
        if (_.isEmpty(form.username) || _.isEmpty(form.email)) {
            return false;
        }

        var slug = slugify(form.username, {lower: true});

        var usersByUsernameCount = await(User.count({slug: slug}));
        var usersByEmailCount = await(User.count({email: form.email}));
        var usersByNewEmailCount = await(User.count({newEmail: form.email}));

        if ((usersByUsernameCount > 0 && slug !== user.slug) || (usersByEmailCount > 0 && form.email !== user.email)
            || (usersByNewEmailCount > 0 && form.email !== user.newEmail)) {
            return false;
        }

        if (!/^[a-zA-Z0-9]+$/.test(form.username)) {
            return false;
        }

        if (!emailValidator.validate(form.email)) {
            return false;
        }

        return true;
    })
};
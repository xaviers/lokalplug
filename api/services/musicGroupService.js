var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    removeMusicGroupRelations: async(function (musicGroup) {
        _.forEach(musicGroup.owners, function (owner) {
            owner.musicGroups.remove(musicGroup.id);
            await(owner.save());
        });
    })
};
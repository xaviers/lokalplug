var _ = require('lodash');

module.exports = {
    filterListQuery: function (filters, queryParams) {
        var queryFilters = {};

        _.forEach(filters.labels, function (filter, attribute) {
            if (!_.isEmpty(queryParams[attribute])) {
                var filterOption = _.filter(filter, function (filter) {
                    return filter.query === queryParams[attribute];
                });

                queryFilters[attribute] = _.head(filterOption).value;
            }
        });

        _.forEach(filters.dateFilter, function (filter, attribute) {
            if (!_.isEmpty(queryParams[attribute])) {
                var filterOption = _.filter(filter, function (filter) {
                    return filter.query === queryParams[attribute];
                });

                queryFilters[attribute] = {'>': _.head(filterOption).value, '<': filters.todayDate};
            }
        });

        return queryFilters;
    }
};

var _ = require('lodash'),
    mod_spawnasync = require('spawn-async'),
    os = require('os');

module.exports = {
    createMixtapeFile: function (mixtapeId, mixtapePrefix, mixtapeTitle, coverUrl, tracks, logger) {
        var worker = mod_spawnasync.createWorker({'log': logger});

        var environment = sails.config.environment !== 'production' ? 'devel/'+os.hostname()+'/' : '';

        var argv = ['sh', 'compressTracks.sh', '-e', environment, '-i', mixtapePrefix, '-m', mixtapeTitle, '-u', coverUrl];

        _.forEach(tracks, function (track) {
            argv.push('-u', track.url);
        });

        worker.aspawn(argv,
            function (err, stdout) {
                if (err) {
                    console.log('error: %s', err.message);
                } else {
                    var mixtapeFileUrl = 'https://s3.amazonaws.com/lokalplug/'+ environment +'mixtapes/' + mixtapePrefix + '-' + mixtapeTitle + '_LokalPlug.com.zip';
                    worker.destroy();
                    Mixtape.update({id: mixtapeId}, {fileUrl: mixtapeFileUrl, isFileAvailable: true}).exec(function (err) {
                        if (err) {
                            console.log(err);
                        }
                    });
                }
            });
    },

    convertS3Url: function (originalUrl) {
        var segment = /s3.amazonaws.com([a-zA-Z-_0-9.\/]+)/g.exec(originalUrl)[1];
        segment = _.replace(segment, '/lokalplug', '');

        return 'https://s3.amazonaws.com/lokalplug' + segment;
    }

};
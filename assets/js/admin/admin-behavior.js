var unconfirmedMixtapesPopulator;
var allMixtapesPopulator;
var allArtistsPopulator;
var allMusicGroupsPopulator;

$(function () {
    unconfirmedMixtapesPopulator = $('#unconfirmedMixtapes').tablePopulator({
        fetch_url: "/unconfirmed-mixtapes",
        previous_button_selector: "#PreviousUnconfirmedMixtapes",
        next_button_selector: "#NextUnconfirmedMixtapes",
        pagination_limit: 10,
        row_mapper: function (mixtape, row) {
            row[0] = '<img src="' + mixtape.coverUrl + '" width="70px" height="70px">';
            row[1] = mixtape.title;
            row[2] = mixtape.confirmationCode;
            row[3] = _.join(_.map(_.concat(mixtape.artists, mixtape.beatmakers, mixtape.musicGroups), 'name'), ',');
            row[4] = mixtape.uploader.username;
            row[5] = mixtape.createdAt;
            row[6] = '<button class="btn btn-success action-button confirm-mixtape-button" onclick="confirmMixtape(\'' + mixtape.slug + '\')"><i class="fa fa-check" aria-hidden="true"></i></button>';
            row[6] += '<a href="/mixtape/' + mixtape.slug + '" class="btn btn-primary action-button" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            row[6] += '<button class="btn btn-danger action-button" onclick="requestMixtapeDeletion(\'' + mixtape.slug + '\')"><i class="fa fa-times" aria-hidden="true"></i></button>';
        }
    });

    allMixtapesPopulator = $('#allMixtapes').tablePopulator({
        fetch_url: "/all-mixtapes",
        previous_button_selector: "#PreviousMixtapes",
        next_button_selector: "#NextMixtapes",
        pagination_limit: 10,
        row_mapper: function (mixtape, row) {
            row[0] = '<img src="' + mixtape.coverUrl + '" width="70px" height="70px">';
            row[1] = mixtape.title;
            row[3] = _.join(_.map(_.concat(mixtape.artists, mixtape.beatmakers, mixtape.musicGroups), 'name'), ',');
            row[4] = mixtape.uploader.username;
            row[5] = mixtape.createdAt;
            row[6] = mixtape.isOfficial === true ? 'Oui' : 'Non';
            row[7] = mixtape.isConfirmed === true ? 'Oui' : 'Non';
            row[8] = mixtape.isFileAvailable === true ? 'Oui' : 'Non';
            row[9] = '<a href="/mixtape/' + mixtape.slug + '" class="btn btn-primary action-button" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            row[9] += '<button class="btn btn-danger action-button" onclick="requestMixtapeDeletion(\'' + mixtape.slug + '\')"><i class="fa fa-times" aria-hidden="true"></i></button>';
        }
    });

    allArtistsPopulator = $('#allArtists').tablePopulator({
        fetch_url: "/all-artists",
        previous_button_selector: "#PreviousArtists",
        next_button_selector: "#NextArtists",
        pagination_limit: 10,
        row_mapper: function (artist, row) {
            row[0] = '<img src="' + artist.pictureUrl + '" width="70px" height="70px">';
            row[1] = artist.name;
            row[2] = _.upperFirst(artist.type);
            row[3] = artist.lastOfficialActivityAt;
            row[4] = artist.officialMixtapesCount;
            row[5] = artist.officialTracksCount;
            row[6] = artist.musicGroups.length > 0 ? _.join(_.map(artist.musicGroups, 'name'), ',') : 'Aucun';
            row[7] = _.join(_.map(artist.owners, 'username'), ',');
            row[8] = artist.createdAt;
            row[9] = '<a href="/artist/' + artist.slug + '" class="btn btn-primary action-button" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            row[9] += '<button class="btn btn-danger action-button" onclick="requestArtistDeletion(\'' + artist.slug + '\')"><i class="fa fa-times" aria-hidden="true"></i></button>';
        }
    });

    allBeatmakersPopulator = $('#allBeatmakers').tablePopulator({
        fetch_url: "/all-beatmakers",
        previous_button_selector: "#PreviousBeatmakers",
        next_button_selector: "#NextBeatmakers",
        pagination_limit: 10,
        row_mapper: function (beatmaker, row) {
            row[0] = '<img src="' + beatmaker.pictureUrl + '" width="70px" height="70px">';
            row[1] = beatmaker.name;
            row[2] = beatmaker.lastOfficialActivityAt;
            row[3] = beatmaker.officialMixtapesCount;
            row[4] = beatmaker.officialTracksCount;
            row[5] = beatmaker.musicGroups.length > 0 ? _.join(_.map(beatmaker.musicGroups, 'name'), ',') : 'Aucun';
            row[6] = _.join(_.map(beatmaker.owners, 'username'), ',');
            row[7] = beatmaker.createdAt;
            row[8] = '<a href="/beatmaker/' + beatmaker.slug + '" class="btn btn-primary action-button" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            row[8] += '<button class="btn btn-danger action-button" onclick="requestBeatmakerDeletion(\'' + beatmaker.slug + '\')"><i class="fa fa-times" aria-hidden="true"></i></button>';
        }
    });

    allMusicGroupsPopulator = $('#allMusicGroups').tablePopulator({
        fetch_url: "/all-music-groups",
        previous_button_selector: "#PreviousMusicGroups",
        next_button_selector: "#NextMusicGroups",
        pagination_limit: 10,
        row_mapper: function (musicGroup, row) {
            row[0] = '<img src="' + musicGroup.pictureUrl + '" width="70px" height="70px">';
            row[1] = musicGroup.name;
            row[2] = musicGroup.lastOfficialActivityAt;
            row[3] = musicGroup.officialMixtapesCount;
            row[4] = musicGroup.officialTracksCount;
            row[5] = _.concat(musicGroup.artists, musicGroup.beatmakers).length;
            row[6] = _.join(_.map(musicGroup.owners, 'username'), ',');
            row[7] = musicGroup.createdAt;
            row[8] = '<a href="/artist/' + musicGroup.slug + '" class="btn btn-primary action-button" role="button"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            row[8] += '<button class="btn btn-danger action-button" onclick="requestMusicGroupDeletion(\'' + musicGroup.slug + '\')"><i class="fa fa-times" aria-hidden="true"></i></button>';
        }
    });
});

function reloadUnconfirmedMixtapesTable() {
    unconfirmedMixtapesPopulator.tablePopulator('reload');
}

function reloadAllMixtapesTable() {
    allMixtapesPopulator.tablePopulator('reload');
}

function reloadAllArtistsTable() {
    allArtistsPopulator.tablePopulator('reload');
}

function reloadAllBeatmakersTable() {
    allBeatmakersPopulator.tablePopulator('reload');
}

function reloadAllMusicGroupsTable() {
    allMusicGroupsPopulator.tablePopulator('reload');
}

function confirmMixtape(slug) {
    $.post('/confirm-mixtape/' + slug)
        .done(function () {
            notifySuccess('Mixtape confirmée');
        })
        .fail(function () {
            notifyDanger('Une erreur est survenue');
        })
        .always(function () {
            reloadUnconfirmedMixtapesTable();
        });
}

function requestMixtapeDeletion(slug) {
    deleteAlert(slug, deleteMixtape, reloadAllMixtapesTable);
}

function requestArtistDeletion(slug) {
    deleteAlert(slug, deleteArtist, reloadAllArtistsTable);
}

function requestBeatmakerDeletion(slug) {
    deleteAlert(slug, deleteBeatmaker, reloadAllBeatmakersTable);
}

function requestMusicGroupDeletion(slug) {
    deleteAlert(slug, deleteMusicGroup, reloadAllMusicGroupsTable);
}

function deleteAlert(slug, earlyCb, laterCb) {
    swal({
            title: 'Êtes-vous sûr ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: 'Annuler',
            confirmButtonText: 'Oui, supprimer'
        },
        function(isConfirm){
            if (isConfirm)
                earlyCb(slug, laterCb);
        });
}
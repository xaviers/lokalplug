var ArtistPicker = function (selectElement) {
    var picker = this;
    selectElement.after($('#artistPickerTemplate').html());
    picker.select = selectElement.appendTo(selectElement.parent().find('.artist-picker-dropdown'));
    picker.dropdown = picker.select.parents('.artist-picker-dropdown');

    picker.find = function (selector) {
        return $(picker.dropdown).find(selector);
    };

    picker.getDropdown = function () {
        return $(picker.dropdown);
    };

    picker.find('.artist-missing a').click(function () {
        $('#createArtistModal').modal('show');
    });

    picker.find('.dropdown-toggle').click(function () {
        picker.focus();
    });

    picker.find('input[type=text]').on('input', function () {
        picker.fetchResults($(this).parents('.artist-picker-dropdown'));
    });

    picker.find('input[type=text]').keyup(function (e) {
        if (e.keyCode === 46)
            picker.fetchResults($(this).parents('.artist-picker-dropdown'));
    });

    picker.getDropdown().click(function (e) {
        if ($(this).hasClass('open') && !$(e.target).parents('.dropdown-toggle').length)
            e.stopPropagation();
    });

    picker.focus = function () {
        setTimeout(function () {
            picker.getDropdown().find('input[type=text]').focus();
        }, 0);
    };

    picker.onUnselectedItemClick = function (item) {
        if (!picker.select.prop("multiple")) {
            if (!_.isEmpty(picker.find('.dropdown-menu.inner li.selected'))) {
                picker.find('.dropdown-menu.inner li.selected').each(function () {
                    picker.onSelectedItemClick($(this));
                });
            }
        }

        var artistList = picker.find('.dropdown-menu.inner');

        if (_.isEmpty(artistList.find('li.selected'))) {
            artistList.append('<li class="divider"></li><li class="dropdown-header selected-header"><span class="text">' + artistPickerText.selected + '</span></li>');
        }

        item.clone().removeClass('unselected').addClass('selected').appendTo(artistList);
        item.hide();

        picker.updateToggleText();
        picker.updateSelect();
        picker.removeUnnecessaryHeaders();
        picker.focus();
    };

    picker.onSelectedItemClick = function (item) {
        var id = item.data('id');
        var type = item.data('type');
        var artistList = picker.find('.dropdown-menu.inner');

        artistList.find('li[data-id="' + id + '"]').show();
        item.remove();

        if (!picker.find('.dropdown-menu.inner .' + type + '-header').length) {
            $('<li class="dropdown-header result-header '+ type +'-header">'+ artistPickerText.headers[type] + '</li>').insertBefore('li[data-id="' + id + '"]');
        }

        if (_.isEmpty(artistList.find('li.selected'))) {
            artistList.find('.divider').remove();
            artistList.find('.selected-header').remove();
        }

        picker.updateToggleText();
        picker.updateSelect();
        picker.focus();
    };

    picker.updateToggleText = function () {
        var names = [];

        picker.find('.dropdown-toggle .filter-option').remove();

        picker.find('.dropdown-menu.inner li.selected').each(function () {
            names.push($(this).data('name'));
        });

        if (!_.isEmpty(names)) {
            picker.find('.dropdown-toggle').prepend('<span class="filter-option pull-left">' + _.join(names, ', ') + '</span>');
            picker.find('.dropdown-toggle .placeholder').hide();
        }
        else {
            picker.find('.dropdown-toggle .placeholder').show();
        }
    };

    picker.updateSelect = function () {
        picker.find('select option').remove();

        picker.find('.dropdown-menu.inner li.selected').each(function () {
            picker.find('select').append('<option value="' + $(this).data('id') + '" data-slug="' + $(this).data('slug') + '" selected="selected">' + $(this).data('name') + '</option>');
        });
    };

    picker.addArtistToList = function (artist, type) {
        if (!_.isEmpty(picker.find('.dropdown-menu.inner li[data-id="' + artist.id + '"]')))
            return;

        picker.find('.dropdown-menu.inner').prepend('<li class="unselected" data-id="' + artist.id + '" data-slug="' + artist.slug + '" data-name="' + artist.name + '" data-type="' + type + '"><a>' +
            '<div class="artist-picture-container">' +
            '<span class="avatar"><img src="' + artist.pictureUrl + '"></span>' +
            artist.name + '</div>' +
            '<span class="fa fa-check check-mark"></span>' +
            '</a></li>');
    };

    picker.removeUnnecessaryHeaders = function () {
        if (!picker.find('.dropdown-menu.inner .unselected[data-type="artist"]:visible').length) {
            picker.find('.dropdown-menu.inner .artist-header').remove();
        }

        if (!picker.find('.dropdown-menu.inner .unselected[data-type="dj"]:visible').length) {
            picker.find('.dropdown-menu.inner .dj-header').remove();
        }

        if (!picker.find('.dropdown-menu.inner .unselected[data-type="beatmaker"]:visible').length) {
            picker.find('.dropdown-menu.inner .beatmaker-header').remove();
        }

        if (!picker.find('.dropdown-menu.inner .unselected[data-type="music-group"]:visible').length) {
            picker.find('.dropdown-menu.inner .music-group-header').remove();
        }
    };

    picker.fetchResults = function () {
        var url = picker.select.data('url');
        var q = picker.find('input[type=text]').val();

        picker.find('.dropdown-menu.inner .unselected').remove();
        picker.find('.dropdown-menu.inner .result-header').remove();

        if (_.isEmpty(q))
            return;

        picker.getDropdown().addClass('fetching');
        picker.find('.status.status-loading').show();
        picker.find('.status.status-initialized').hide();
        picker.find('.status.status-error').hide();

        $.get({url: url, data: {q: q}, dataType: 'json'})
            .done(function (results) {
                picker.find('.status.status-initialized').show();
                picker.find('.status.status-loading').hide();
                picker.find('.status.status-error').hide();

                _.forEach(_.reverse(results.musicGroups), function (musicGroup) {
                    picker.addArtistToList(musicGroup, 'music-group');
                });

                if (!_.isEmpty(results.musicGroups)) {
                    picker.find('.dropdown-menu.inner').prepend('<li class="dropdown-header result-header music-group-header">' + artistPickerText.headers['music-group'] +'</li>');
                }

                _.forEach(_.reverse(results.beatmakers), function (beatmaker) {
                    picker.addArtistToList(beatmaker, 'beatmaker');
                });

                if (!_.isEmpty(results.beatmakers)) {
                    picker.find('.dropdown-menu.inner').prepend('<li class="dropdown-header result-header beatmaker-header">' + artistPickerText.headers['beatmaker'] +'</li>');
                }

                _.forEach(_.reverse(results.djs), function (dj) {
                    picker.addArtistToList(dj, 'dj');
                });

                if (!_.isEmpty(results.djs)) {
                    picker.find('.dropdown-menu.inner').prepend('<li class="dropdown-header result-header dj-header">' + artistPickerText.headers['dj'] +'</li>');

                }

                _.forEach(_.reverse(results.artists), function (artist) {
                    picker.addArtistToList(artist, 'artist');
                });

                if (!_.isEmpty(results.artists)) {
                    picker.find('.dropdown-menu.inner').prepend('<li class="dropdown-header result-header artist-header">' + artistPickerText.headers['artist'] +'</li>');
                }

                picker.removeUnnecessaryHeaders();
                picker.bindClick();
            })
            .fail(function () {
                picker.find('.status.status-error').show();
                picker.find('.status.status-initialized').hide();
                picker.find('.status.status-loading').hide();
            })
            .always(function () {
                picker.getDropdown().removeClass('fetching');
            });
    };

    picker.bindClick = function () {
        var items = picker.find('.dropdown-menu.inner li');

        items.off('click');

        items.click(function () {
            if ($(this).hasClass('unselected'))
                picker.onUnselectedItemClick($(this));
            else
                picker.onSelectedItemClick($(this));
            picker.bindClick();
        });
    };

    picker.get = function () {
        if (picker.select.prop("multiple")) {
            return picker.getMultiple();
        }

        return picker.getOne();
    };

    picker.getOne = function () {
        var artist = picker.select.find('option:selected').first();

        return {
            id: artist.val(),
            slug: artist.data('slug'),
            name: artist.html()
        }
    };

    picker.getMultiple = function () {
        var artists = [];

        picker.select.find('option:selected').each(function () {
            artists.push({
                id: $(this).val(),
                slug: $(this).data('slug'),
                name: $(this).html()
            })
        });

        return artists;
    };
};

$(document).keyup(function (e) {
    if (e.keyCode === 27) {
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
    }
});
$(function () {
    adjustContentContainerHeight();
    $('[data-toggle="popover"]').popover();

    $('.pagination li a').each(function() {
        if ($(this).parent().hasClass('disabled')) {
            $(this).attr('href', '#');
            return;
        }

        var uri = URI(window.location.href);

        uri.removeSearch('page');
        uri.addSearch('page', $(this).data('page'));

        $(this).attr('href', uri);
    });

    $('.card-title a').ellipsis();
});

$(window).resize(function() {
    adjustContentContainerHeight();
});

$('.popover-dismiss').popover({
    trigger: 'focus'
});

$('[data-toggle="tooltip"]').tooltip();

function adjustContentContainerHeight() {
    $('.main-container .content').css("min-height", $(window).height()-196 + "px" );
}

function renderTrackUI(track) {
    track.find('.progress-bar-container').show();
    var $volumeContainer = track.find('.track-volume-container');

    $("<div class='volume'><input type='range' id='trackPlayerVolume' class='volume'/></div>").appendTo($volumeContainer);

    var trackPlayerVolumeStepsCount = $('.track-volume-container').width()/6;
    var trackPlayerVolume = new VolumeControl("#trackPlayerVolume", trackPlayerVolumeStepsCount);
    trackPlayerVolume.renderUI(VolumeControl.getDefaultVolumeValue());
}

var VolumeControl = function(selector, stepsCount) {
    this.$input = $(selector);
    this.$input.hide();
    this.steps = stepsCount;

    if (this.steps > 10)
        this.steps = 10;

    this.$slider = $("<div class='volume-slider'><div class='volume-slider-bar'></div><ul class='volume-slider-sticks'></div>").appendTo(this.$input.parent());

    for (var i = 0; i < this.steps && i < (this.$input.parent().width()-5)/6; i++) {
        var $stick = $('<li><div class="volume-slider-stick"></div></li>').appendTo(this.$slider.find('.volume-slider-sticks'));
        $stick.on('mouseenter', function () {
            $(this).addClass('active');
        }).on('mouseleave', function () {
            $(this).removeClass('active');
        });
    }

    this.$slider.on('mousedown', this, VolumeControl.onDragEvent);

    this.renderUI = function(percent) {
        var index = Math.round(percent * this.steps);
        index = index < this.steps ? index : this.steps;
        this.$slider.find('.volume-slider-sticks > li').find('div').css('opacity', 0);

        for (var i = 0; i < index; i++) {
            this.$slider.find('.volume-slider-sticks > li:eq(' + i + ')').find('div').css('opacity', 1);
        }
    };

    this.getPercent = function(event) {
        var percent = (event.pageX - this.$slider.offset().left) / this.$slider.find('.volume-slider-sticks').width();
        percent = percent >= 0 ? percent : 0;
        percent = percent <= 1 ? percent : 1;
        return percent;
    }
};

VolumeControl.getDefaultVolumeValue = function() {
    return Howler.volume();
};

VolumeControl.onDragEvent = function(event) {
    var volumeControl = event.data;
    volumeControl.renderUI(volumeControl.getPercent(event));
    Howler.volume(volumeControl.getPercent(event));
    $(document.body).on('mousemove', function (event) {
        volumeControl.renderUI(volumeControl.getPercent(event));
        Howler.volume(volumeControl.getPercent(event));
    });
    $(document.body).on('mouseup', function () {
        $(document.body).off('mouseup');
        $(document.body).off('mousemove');
    });
};
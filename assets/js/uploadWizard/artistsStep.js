var mixtapeArtists;

$(document).ready(function () {
    mixtapeArtists = $('#mixtapeArtists').artistPicker();

    $('#artists').submit(function (e) {
        e.preventDefault();
        processArtistsStep();
    });

    $('#mixtapeIsOfficial').on('changed.bs.select', function () {

        if (this.selectedIndex === 1) {
            swal(officialMixtapeInfoTitle, officialMixtapeInfoMessage, 'info');
        }
    });
});

function processArtistsStep() {
    mixtape.isOfficial = $('#mixtapeIsOfficial').val();

    _.forEach(mixtapeArtists.get(), function (artist) {
        if (_.indexOf(_.map(mixtape.artists, 'id'), artist.id) === -1) {
            mixtape.artists.push(artist);
        }
    });

    nextStep($('#artists'));
}
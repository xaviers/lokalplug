function generateSummary() {
    var detailsSummary = $('.details-summary');
    $('.tracks-summary tbody').empty();

    detailsSummary.find('.mixtape-title').html(mixtape.title);
    detailsSummary.find('.mixtape-cover').html("<img src='"+mixtape.coverUrl+"'>");
    detailsSummary.find('.mixtape-authors').html(_.join(_.map(mixtape.artists, 'name'), ', '));
    detailsSummary.find('.mixtape-genres').html(_.join(_.map(mixtape.musicGenres, 'name'), ', '));
    detailsSummary.find('.mixtape-release-date').html(mixtape.releaseDate);
    detailsSummary.find('.mixtape-is-official').html(mixtape.isOfficial === "true" ? itsOfficial : itsNotOfficial);

    _.forEach(mixtape.tracks, function (track) {
        $('.tracks-summary').find('tbody:last-child').append(
            '<tr>'
            + '<td>' + track.number + '</td>'
            + '<td>' + track.title + '</td>'
            + '<td>' + track.artist.name + '</td>'
            + '<td>' + _.join(_.map(track.featuringArtists, 'name'), ', ') + '</td>'
            + '<td>' + _.join(_.map(track.beatmakers, 'name'), ', ') + '</td>'
            + '</tr>');
    });

    $('html, body').animate({
        scrollTop: $(".account-content-column").offset().top
    }, 500);
}
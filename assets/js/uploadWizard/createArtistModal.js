var artistMusicGroups;
var musicGroupArtists;

$(document).ready(function () {
    artistMusicGroups = $('#artistMusicGroups').artistPicker();
    musicGroupArtists = $('#musicGroupArtists').artistPicker();

    var artistModal = $('#createArtistModal');

    artistModal.modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    });

    $('#addArtistForm').submit(function (e) {
        e.preventDefault();
        addArtist();
    });

    artistModal.on('hidden.bs.modal', function () {
        var form = $('#addArtistForm');

        $('#createArtistModal').find('.alert').each(function() {
            $(this).hide();
        });

        form.find('.help-block').each(function() {
            $(this).html('');
        });

        form.find('.has-error').removeClass('has-error');
        form.find('.has-danger').removeClass('has-danger');

        form.find('#artistName').val('');
        form.find('#artistType').selectpicker('val', '');
        form.find('#artistMusicGroups').val('');
        form.find('#musicGroupArtist').val('');

        form.find('.music-groups-select-container').hide();
        form.find('.all-artists-except-groups-select-container').hide();

        form.show();
    });

    $('#artistType').on('changed.bs.select', function () {
        if (this.selectedIndex === 1) {
            $('.all-artists-except-groups-select-container').show('fast');
            $('.music-groups-select-container').hide('fast');
        }
        else if (!_.isUndefined(this.selectedIndex)) {
            $('.music-groups-select-container').show('fast');
            $('.all-artists-except-groups-select-container').hide('fast');
        }
    });
});

function addArtist() {
    var modelPath;
    var artist = {
        name: $('#artistName').val(),
        musicGroups: _.map(artistMusicGroups.get(), 'id'),
        artists: _.map(musicGroupArtists.get(), 'id')
    };

    var artistType = $('#artistType').val();

    if (artistType === 'artist') {
        modelPath = 'artists';
        artist.type = 'artist';
    } else if (artistType === 'dj') {
        modelPath = 'artists';
        artist.type = 'dj';
    } else if (artistType === 'music-group') {
        modelPath = 'music-groups';
    } else {
        modelPath = 'beatmakers'
    }

    $.ajax({
        url: '/'+modelPath+'/create',
        type: 'POST',
        data: artist,
        success: function() {
            $('#createArtistModal').find('.alert-success').show();
        },
        error: function() {
            $('#createArtistModal').find('.alert-danger').show();
        },
        complete: function () {
            $('#addArtistForm').hide();
        }
    });
}
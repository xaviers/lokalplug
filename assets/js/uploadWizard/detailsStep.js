var mustUploadCover = true;

$(document).ready(function () {
    var mixtapeCoverInput = $("#mixtapeCover")[0];
    mixtapeCoverInput.onclick = function () {
        this.value = null;
    };
    mixtapeCoverInput.onchange = function () {
        mustUploadCover = true;
    };

    $('#details').submit(function (e) {
        e.preventDefault();
        processDetailsStep();
    });

    $('#mixtapeReleaseDate').datepicker({
        language: currentLocale,
        endDate: currentDate
    });
});

function processDetailsStep() {
    mixtape.title = $('#mixtapeTitleField').val();
    mixtape.releaseDate = $('#mixtapeReleaseDate').val();

    $('#mixtapeMusicGenres').closest('.bootstrap-select').find('li.selected').each(function () {
        var selectedOption = $($('#mixtapeMusicGenres').find('option').get($(this).data('original-index')));

        if (_.indexOf(_.map(mixtape.musicGenres, 'id'), selectedOption.val()) === -1) {
            mixtape.musicGenres.push({
                id: selectedOption.val(),
                name: selectedOption.html()
            });
        }
    });

    if (mustUploadCover) {
        $('.loading-container').fadeIn('slow');

        $.ajax({
            url: '/upload/mixtape-cover',
            type: 'POST',
            data: new FormData($('#details')[0]),
            processData: false,
            contentType: false,
            success: function (data) {
                mixtape.coverUrl = data.coverUrl;

                nextStep($('#details'));
                mustUploadCover = false;
            },
            complete: function () {
                $('.loading-container').fadeOut('slow');
            },
            dataType: 'json'
        });
    } else {
        nextStep($('#details'));
    }
}
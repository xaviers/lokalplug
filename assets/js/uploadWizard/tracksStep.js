var trackOwner;
var trackFeaturings;
var trackBeatmakers;

$(document).ready(function () {
    trackOwner = $('#trackOwner').artistPicker();
    trackFeaturings = $('#trackFeaturings').artistPicker();
    trackBeatmakers = $('#trackBeatmakers').artistPicker();

    $('#addTrackForm').submit(function (e) {
        e.preventDefault();
        addTrack();
    });

    $('.wizard-pagination .next-button').click(function () {
        processTrackStep();
    });

    $('#trackNumber').TouchSpin({
        min: 1,
        max: 1,
        step: 1
    });
});

function processTrackStep() {
    if (mixtape.tracks.length > 0) {
        generateSummary();
        nextStep($('#addTrackForm'));
    }
}

function addTrack() {
    var trackNumber = $('#trackNumber');
    var trackTitle = $('#trackTitle');

    var track = {
        number: trackNumber.val(),
        title: trackTitle.val(),
        artist: trackOwner.get(),
        featuringArtists: [],
        beatmakers: []
    };

    _.forEach(trackFeaturings.get(), function (artist) {
        track.featuringArtists.push(artist);
    });

    _.forEach(trackBeatmakers.get(), function (artist) {
        track.beatmakers.push(artist);
    });

    mixtape.tracks[trackNumber.val() - 1] = track;

    trackNumber.trigger("touchspin.updatesettings", {max: mixtape.tracks.length + 1});
    trackNumber.trigger("touchspin.uponce");
    trackTitle.val('');

    reloadTable();
}

function reloadTable() {
    $('#mixtapeTracks').find('tbody').empty();

    _.forEach(mixtape.tracks, function (track) {
        $('#mixtapeTracks').find('tbody:last-child').append(
            '<tr>'
            + '<td>' + track.number + '</td>'
            + '<td>' + track.title + '</td>'
            + '<td>' + track.artist.name + '</td>'
            + '<td>' + _.join(_.map(track.featuringArtists, 'name'), ', ') + '</td>'
            + '<td>' + _.join(_.map(track.beatmakers, 'name'), ', ') + '</td>'
            + '</tr>');
    });
}
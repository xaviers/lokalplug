var trackToUploadIndex = 0;

$(document).ready(function () {
    $('#uploadTracksModal').on('shown.bs.modal', function () {
        _.forEach(mixtape.tracks, function (track) {
            $('#tracksToUpload').find('tbody:last-child').append(
                '<tr>'
                + '<td>' + track.number + '</td>'
                + '<td>' + track.title + '</td>'
                + '<td>'
                + '<form class="upload-track-form" role="form" data-is-uploaded="false" data-track-index="' + (track.number - 1) + '">'
                + '<input type="hidden" name="trackNumber" value="' + ("0" + track.number).slice(-2) + '">'
                + '<input type="hidden" name="mixtapeTitle" value="' + mixtape.title + '">'
                + '<input type="hidden" name="trackTitle" value="' + track.title + '">'
                + '<input type="hidden" name="trackArtist" value="' + (!_.isEmpty(track.artist) ? track.artist.name : null) + '">'
                + '<input type="hidden" name="trackMusicGroup" value="' + (!_.isEmpty(track.musicGroup) ? track.musicGroup.name : null) + '">'
                + '<input type="hidden" name="trackFeaturing" value="' + _.join(_.map(track.featuringArtists, 'name'), '-') + '">'
                + '<input type="hidden" name="trackBeatmakers" value="' + _.join(_.map(track.beatmakers, 'name'), '-') + '">'
                + '<input name="track" type="file" required>'
                + '</form>'
                + '</td>'
                + '<td><span class="upload-status">'+ trackWaitingStatus +'</span></td>'
                + '</tr>');
        });
    });

    $('#startUpload').click(function () {
        uploadTrack();
    });
});

function checkUploads() {
    var trackUploaded = true;

    $('.upload-track-form').each(function () {
        if ($(this).data('isUploaded') !== 'true')
            trackUploaded = false;
    });

    if (trackUploaded) {
        $('#uploadTracksModal').modal('hide');
        createMixtape();
    } else {
        trackToUploadIndex++;
        uploadTrack();
    }
}

function uploadTrack() {
    $('#startUpload').prop('disabled', true);

    var uploadForm = $('.upload-track-form[data-track-index="' + trackToUploadIndex + '"]');
    uploadForm.parents('tr').find('.upload-status').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i> ' + trackUploadingStatus);

    $.ajax({
        url: '/upload/mixtape-track',
        type: 'POST',
        data: new FormData(uploadForm[0]),
        processData: false,
        contentType: false,
        success: function (data) {
            mixtape.tracks[trackToUploadIndex].url = data.trackUrl;
            uploadForm.parents('tr').find('.upload-status').html('<i class="fa fa-check"></i> ' + trackSuccessStatus);
            uploadForm.data('isUploaded', 'true');
            checkUploads();
        },
        error: function () {
            uploadForm.find('input[name="track"]').prop('disabled', false);
            uploadForm.parents('tr').find('.upload-status').html('<i class="fa fa-exclamation-triangle"></i> <button type="button" class="btn btn-default btn-xs retry-track-upload" onclick="uploadTrack();">Reesayer</button>');
        },
        dataType: 'json'
    });

    uploadForm.find('input[name="track"]').prop('disabled', true);
}
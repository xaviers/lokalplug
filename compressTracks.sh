#!/bin/bash

while getopts "e:i:m:u:" arg; do
  case ${arg} in
    e)
      environment=$OPTARG;;
    i)
      mixtapeId=$OPTARG
      path=./.tmp/mixtapeUpload/${mixtapeId}
      mkdir -p ${path};;
    m)
      mixtape=$OPTARG;;
    u)
      wget -q $OPTARG -P ${path};;
  esac
done

mixtapeFileName=${mixtapeId}-${mixtape}_LokalPlug.com.zip

cd ${path}
for x in `find . -iregex '.*\.\(jpg\|jpeg\|png\|JPG\|JPEG\|PNG\)' -printf '%f\n'`;do mv ${x} 00-cover.${x##*.}; done
for x in `find . -iregex '.*\.\(mp3\|m4a\|MP3\|M4A\)' -printf '%f\n'`;do mv ${x} "$(echo ${x} | sed -r 's/^[a-zA-Z0-9]+-'${mixtape}-'//')" ; done
zip -q ${mixtapeFileName} *

s3Secret=1X5Af2R+xE5JM17otfrCiGtbeWZTHHj6z8XosU4W
s3Key=AKIAJGINGMYIDXNLGXPA
s3Bucket=lokalplug
s3Path=${environment}mixtapes/

cd ../ && python3 upload.py ${mixtapeId} ${mixtapeFileName} ${s3Bucket} ${s3Path} ${s3Key} ${s3Secret}

echo https://s3.amazonaws.com/${s3Bucket}/${s3Path}${mixtapeFileName}

rm -r ${mixtapeId}
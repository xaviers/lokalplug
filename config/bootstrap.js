var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    opn = require('opn'),
    _ = require('lodash');

module.exports.bootstrap = async(function (cb) {

    if (sails.config.environment === 'production') {
        var express = require("express");
        var app = express();
        app.get('*', function(req,res) {
            if(req.isSocket)
                return res.redirect('wss://' + req.headers.host + req.url);
            return res.redirect('https://' + req.headers.host + req.url);
        }).listen(80);
    }

    if (await(User.count({email: 'xsaliniere@lokalplug.com'})) === 0 && await(User.count({slug: 'lexav'})) === 0) {
        sails.log.debug('Creating admin user (lexav)');

        await(User.create({
            username: 'LeXav',
            slug: 'lexav',
            email: 'lexav@lokalplug.com',
            password: 'admin97231',
            active: true,
            isUploader: true,
            isAdmin: true
        }));
    }

    if (await(User.count({email: 'feleybeats@gmail.com'})) === 0 && await(User.count({slug: 'kstoner'})) === 0) {
        sails.log.debug('Creating admin user (kstoner)');

        await(User.create({
            username: 'KStoner',
            slug: 'kstoner',
            email: 'feleybeats@gmail.com',
            password: 'admin97212',
            active: true,
            isUploader: true,
            isAdmin: true
        }));
    }

    if (await(User.count({email: 'test@lokalplug.com'})) === 0) {
        sails.log.debug('Creating test user');

        await(User.create({
            username: 'testUser',
            slug: 'test-user',
            email: 'test@lokalplug.com',
            password: 'test97231',
            active: true,
            isUploader: false,
            isAdmin: false
        }));
    }

    if (await(Language.count()) === 0) {
        sails.log.debug('Creating languages');

        await(Language.create({
            name: 'French',
            languageCode: 'fr',
            countryCode: 'fr',
            localeCode: 'fr_FR'
        }));

        await(Language.create({
            name: 'English',
            languageCode: 'en',
            countryCode: 'gb',
            localeCode: 'en_US'
        }));
    }

    if (await(MusicGenre.count()) === 0) {
        sails.log.debug('Creating music genres');

        await(MusicGenre.create({
            name: 'Rap/Trap',
            slug: 'rap-trap',
            color: '#c0202f'
        }));

        await(MusicGenre.create({
            name: 'Reggae',
            slug: 'reggae',
            color: '#008000'
        }));

        await(MusicGenre.create({
            name: 'Dancehall',
            slug: 'dancehall',
            color: '#daa520'
        }));

        await(MusicGenre.create({
            name: 'Bouyon',
            slug: 'bouyon',
            color: '#ff7f50'
        }));

        await(MusicGenre.create({
            name: 'Zouk',
            slug: 'zouk',
            color: '#468499'
        }));

        await(MusicGenre.create({
            name: 'Other',
            slug: 'other',
            color: '#808080'
        }));
    }

    if (await(MixtapeType.count()) === 0) {
        sails.log.debug('Creating mixtape types');

        await(MixtapeType.create({
            name: 'Artist Mixtape',
            slug: 'artist-mixtape'
        }));

        await(MixtapeType.create({
            name: 'DJ Mixtape',
            slug: 'dj-mixtape'
        }));

        await(MixtapeType.create({
            name: 'Beatmaker Mixtape',
            slug: 'beatmaker-mixtape'
        }));
    }

    if (sails.config.environment === 'development')
        setTimeout(function(){
            opn('http://localhost:'+sails.config.port);
        }, 1000);

    cb();
});

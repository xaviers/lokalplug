module.exports.properties = {
    comingSoon: true,

    defaultAvatarUrl: 'https://lokalplug.com/images/default-avatar.png',
    defaultMusicGroupPictureUrl: 'https://lokalplug.com/images/default-music-group-picture.png',
    defaultBeatmakerPictureUrl: 'https://lokalplug.com/images/default-beatmaker-picture.png',
    defaultDJPictureUrl: 'https://lokalplug.com/images/default-dj-picture.png',
    defaultArtistPictureUrl: 'https://lokalplug.com/images/default-artist-picture.png',
    defaultArtistCoverUrl: 'https://lokalplug.com/images/default-artist-cover.png',

    noReplyMailPassword: 'guavbvfj',

    navLinks: [
        {label: 'home', key: 'home', href: '/'},
        {label: 'mixtapes', key: 'mixtape', href: '/mixtapes'},
        {label: 'artists', key: 'artist', href: '/artists'},
        {label: 'beatmakers', key: 'beatmaker', href: '/beatmakers'},
        {label: 'music-groups', key: 'music-group', href: '/music-groups'}
    ]
};
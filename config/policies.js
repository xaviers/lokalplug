/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

    '*': 'i18nSession',

    'AccountController': {
        '*': ['isAuthenticated', 'i18nSession']
    },

    'ArtistController': {
        'renderAllArtistsAdminView': ['isAdmin', 'i18nSession'],
        'getAllArtists': ['isAdmin', 'i18nSession'],
        'createArtist': ['isAuthenticated', 'i18nSession'],
        'deleteArtist': ['isAuthenticated', 'i18nSession']
    },

    'AuthController': {
        'renderLoginView': ['isNotAuthenticated', 'i18nSession'],
        'login': ['isNotAuthenticated', 'i18nSession'],
        'logout': ['isAuthenticated', 'i18nSession']
    },

    'BeatmakerController': {
        'renderAllBeatmakersAdminView': ['isAdmin', 'i18nSession'],
        'getAllBeatmakers': ['isAdmin', 'i18nSession'],
        'createBeatmaker': ['isAuthenticated', 'i18nSession'],
        'deleteBeatmaker': ['isAuthenticated', 'i18nSession']
    },

    'MixtapeConfirmationController': {
        '*': ['isAdmin', 'i18nSession']
    },

    'MixtapeController': {
        'renderAllMixtapesAdminView': ['isAdmin', 'i18nSession'],
        'getAllMixtapes': ['isAdmin', 'i18nSession'],
        'bookmark': ['isAuthenticated', 'i18nSession'],
        'like': ['isAuthenticated', 'i18nSession'],
        'dislike': ['isAuthenticated', 'i18nSession'],
        'deleteMixtape': ['isAuthenticated', 'i18nSession']
    },

    'MusicGroupController': {
        'renderAllMusicGroupsAdminView': ['isAdmin', 'i18nSession'],
        'getAllMusicGroups': ['isAdmin', 'i18nSession'],
        'createMusicGroup': ['isAuthenticated', 'i18nSession'],
        'deleteMusicGroup': ['isAuthenticated', 'i18nSession']
    },

    'SignupController': {
        'renderView': ['isNotAuthenticated', 'i18nSession'],
        'validateForm': ['isNotAuthenticated', 'i18nSession'],
        'register': ['isNotAuthenticated', 'i18nSession']
    },

    'UploadWizardController': {
        '*': ['isUploader', 'i18nSession']
    },

    'UserSettingsController': {
        'validateBasicSettingsForm': ['isAuthenticated', 'i18nSession'],
        'updateBasicSettings': ['isAuthenticated', 'i18nSession'],
        'resendEmailConfirmation': ['isAuthenticated', 'i18nSession'],
        'cancelEmailUpdate': ['isAuthenticated', 'i18nSession'],
        'uploadNewAvatar': ['isAuthenticated', 'i18nSession'],
        'deleteAvatar': ['isAuthenticated', 'i18nSession'],
        'changePassword': ['isAuthenticated', 'i18nSession'],
    }

    /***************************************************************************
     *                                                                          *
     * Default policy for all controllers and actions (`true` allows public     *
     * access)                                                                  *
     *                                                                          *
     ***************************************************************************/

    // '*': true

    /***************************************************************************
     *                                                                          *
     * Here's an example of mapping some policies to run before a controller    *
     * and its actions                                                          *
     *                                                                          *
     ***************************************************************************/
    // RabbitController: {

    // Apply the `false` policy as the default for all of RabbitController's actions
    // (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
    // '*': false,

    // For the action `nurture`, apply the 'isRabbitMother' policy
    // (this overrides `false` above)
    // nurture	: 'isRabbitMother',

    // Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
    // before letting any users feed our rabbits
    // feed : ['isNiceToAnimals', 'hasRabbitFood']
    // }
};

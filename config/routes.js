module.exports.routes = {

    /*---------- API ----------*/

    'get /api/mixtape/trending': {
        controller: 'ApiController',
        action: 'getTrendingMixtapes',
        cors: true
    },
    'get /api/mixtape/latest-official': {
        controller: 'ApiController',
        action: 'getLatestOfficialMixtapes',
        cors: true
    },
    'get /api/mixtape/latest-uploaded': {
        controller: 'ApiController',
        action: 'getLatestUploadedMixtapes',
        cors: true
    },
    'get /api/mixtape/:slug': {
        controller: 'ApiController',
        action: 'getMixtape',
        cors: true
    },
    'get /api/track': {
        controller: 'ApiController',
        action: 'getTracks',
        cors: true
    },

    /*---------- ADMIN --------*/

    'get /backup-mongo': 'DatabaseController.backupToS3',
    'get /generate-sitemap': 'SitemapController.generate',

    /*---------- WEB ----------*/

    'get /': 'HomepageController.renderView',
    'get /all-mixtapes': 'MixtapeController.getAllMixtapes',
    'get /all-artists': 'ArtistController.getAllArtists',
    'get /all-beatmakers': 'BeatmakerController.getAllBeatmakers',
    'get /all-music-groups': 'MusicGroupController.getAllMusicGroups',
    'get /artists' : 'ArtistController.renderAllArtistsView',
    'get /artist/:artistSlug': 'ArtistController.renderArtistView',
    'get /artists/all-except-beatmakers': 'ArtistController.findAllExceptBeatmakers',
    'get /artists/all-except-groups': 'ArtistController.findAllExceptGroupsByName',
    'get /artists/all-types': 'ArtistController.findAllByName',
    'get /artists/validate-name': 'ArtistController.validateName',
    'get /beatmakers': 'BeatmakerController.renderAllBeatmakersView',
    'get /beatmaker/:beatmakerSlug': 'BeatmakerController.renderBeatmakerView',
    'get /beatmakers/all': 'ArtistController.findBeatmakersByName',
    'get /comingsoon': { view: 'comingsoon' },
    'get /confirm-email/:confirmationId': 'UserSettingsController.confirmEmail',
    'get /contact' : 'ContactController.renderContactView',
    'get /copyright-policy': 'LegalController.renderCopyrightPolicyView',
    'get /login': 'AuthController.renderLoginView',
    'get /logout': 'AuthController.logout',
    'get /me': 'AccountController.renderLatestActivitiesView',
    'get /me/all-mixtapes': 'MixtapeController.renderAllMixtapesAdminView',
    'get /me/all-artists': 'ArtistController.renderAllArtistsAdminView',
    'get /me/all-beatmakers': 'BeatmakerController.renderAllBeatmakersAdminView',
    'get /me/all-music-groups': 'MusicGroupController.renderAllMusicGroupsAdminView',
    'get /me/bookmarked': 'AccountController.renderMixtapesBookmarkedList',
    'get /me/my-mixtapes': 'AccountController.renderMixtapesUploadedList',
    'get /me/settings': 'AccountController.renderSettingsView',
    'get /me/settings/basic-validate': 'UserSettingsController.validateBasicSettingsForm',
    'get /me/settings/cancel-email-update': 'UserSettingsController.cancelEmailUpdate',
    'get /me/settings/delete-avatar': 'UserSettingsController.deleteAvatar',
    'get /me/settings/resend-email-confirmation': 'UserSettingsController.resendEmailConfirmation',
    'get /me/unconfirmed-mixtapes': 'MixtapeConfirmationController.renderUnconfirmedMixtapesView',
    'get /me/upload': 'UploadWizardController.renderView',
    'get /mixtapes' : 'MixtapeController.renderAllMixtapesView',
    'get /mixtape/:mixtapeSlug': 'MixtapeController.renderMixtapeView',
    'get /mixtape/:mixtapeSlug/download': 'MixtapeController.downloadMixtape',
    'get /music-groups': 'MusicGroupController.renderAllMusicGroupsView',
    'get /music-group/:musicGroupSlug': 'MusicGroupController.renderMusicGroupView',
    'get /music-groups/all': 'ArtistController.findMusicGroupsByName',
    'get /privacy-policy': 'LegalController.renderPrivacyPolicyView',
    'get /random-tracks': 'TrackController.getRandomTracks',
    'get /search' : 'SearchController.search',
    'get /signup': 'SignupController.renderView',
    'get /signup/activate/:activationId': 'SignupController.activateAccount',
    'get /signup/validate': 'SignupController.validateForm',
    'get /terms-of-use': 'LegalController.renderTermsOfUseView',
    'get /track/:trackSlug/download': 'TrackController.downloadTrack',
    'get /unconfirmed-mixtapes': 'MixtapeConfirmationController.getUnconfirmedMixtapes',

    'post /artists/create': 'ArtistController.createArtist',
    'post /beatmakers/create': 'BeatmakerController.createBeatmaker',
    'post /confirm-mixtape/:mixtapeSlug': 'MixtapeConfirmationController.confirmMixtape',
    'post /contact': 'ContactController.sendContactMail',
    'post /language/:language': 'LanguageController.switchLang',
    'post /login': 'AuthController.login',
    'post /me/settings/basic-update': 'UserSettingsController.updateBasicSettings',
    'post /me/settings/update-avatar': 'UserSettingsController.uploadNewAvatar',
    'post /me/settings/update-password': 'UserSettingsController.changePassword',
    'post /mixtape/create': 'UploadWizardController.createMixtape',
    'post /mixtape/:mixtapeSlug/bookmark': 'MixtapeController.bookmark',
    'post /mixtape/:mixtapeSlug/dislike': 'MixtapeController.dislike',
    'post /mixtape/:mixtapeSlug/like': 'MixtapeController.like',
    'post /music-groups/create': 'MusicGroupController.createMusicGroup',
    'post /track/:trackSlug/url': 'TrackController.getFileUrl',
    'post /signup': 'SignupController.register',
    'post /upload/mixtape-cover': 'UploadWizardController.uploadCover',
    'post /upload/mixtape-track' : 'UploadWizardController.uploadTrack',

    'delete /mixtape/:mixtapeSlug/delete': 'MixtapeController.deleteMixtape',
    'delete /artist/:artistSlug/delete': 'ArtistController.deleteArtist',
    'delete /beatmaker/:beatmakerSlug/delete': 'BeatmakerController.deleteBeatmaker',
    'delete /music-group/:musicGroupSlug/delete': 'MusicGroupController.deleteMusicGroup'
};

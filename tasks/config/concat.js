/**
 * `concat`
 *
 * ---------------------------------------------------------------
 *
 * Concatenates the contents of multiple JavaScript and/or CSS files
 * into two new files, each located at `concat/production.js` and
 * `concat/production.css` respectively in `.tmp/public/concat`.
 *
 * This is used as an intermediate step to generate monolithic files
 * that can then be passed in to `uglify` and/or `cssmin` for minification.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-concat
 *
 */
module.exports = function (grunt) {

    grunt.config.set('concat', {
        js: {
            src: require('../pipeline').jsFilesToInject,
            dest: '.tmp/public/concat/prod.js'
        },
        jsDependencies: {
            src: require('../pipeline').jsDepFilesToInject,
            dest: '.tmp/public/concat/prod-dependencies.js'
        },
        jsAdmin: {
            src: require('../pipeline').jsAdminFilesToInject,
            dest: '.tmp/public/concat/prod-admin.js'
        },
        jsUploadWizard: {
            src: require('../pipeline').jsUploadWizardFilesToInject,
            dest: '.tmp/public/concat/prod-upload-wizard.js'
        },
        css: {
            src: require('../pipeline').cssFilesToInject,
            dest: '.tmp/public/concat/prod.css'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
};

/**
 * `uglify`
 *
 * ---------------------------------------------------------------
 *
 * Minify client-side JavaScript files using UglifyJS.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-uglify
 *
 */
module.exports = function (grunt) {

    grunt.config.set('uglify', {
        js: {
            src: ['.tmp/public/concat/prod.js'],
            dest: '.tmp/public/min/prod.min.js'
        },
        jsDependencies: {
            src: ['.tmp/public/concat/prod-dependencies.js'],
            dest: '.tmp/public/min/prod-dependencies.min.js'
        },
        jsAdmin: {
            src: ['.tmp/public/concat/prod-admin.js'],
            dest: '.tmp/public/min/prod-admin.min.js'
        },
        jsUploadWizard: {
            src: ['.tmp/public/concat/prod-upload-wizard.js'],
            dest: '.tmp/public/min/prod-upload-wizard.min.js'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
};

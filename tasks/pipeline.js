var path = require('path');

var cssFilesToInject = [
    '/styles/common.css',
    '/styles/layout.css'
];

var jsAdminFilesToInject = [
    '/js/admin/**/*.js'
];

var jsDepFilesToInject = [
    '/js/dependencies/**/*.js'
];

var jsFilesToInject = [
    '/js/*.js'
];

var jsUploadWizardFilesToInject = [
    '/js/uploadWizard/*.js'
];


var tmpPath = '.tmp/public/';

module.exports.cssFilesToInject = mapFiles(cssFilesToInject);

module.exports.jsAdminFilesToInject = mapFiles(jsAdminFilesToInject);

module.exports.jsDepFilesToInject = mapFiles(jsDepFilesToInject);

module.exports.jsFilesToInject = mapFiles(jsFilesToInject);

module.exports.jsUploadWizardFilesToInject = mapFiles(jsUploadWizardFilesToInject);

function mapFiles(files) {
    return files.map(function (filePath) {
        if (filePath[0] === '!') {
            return path.join('!' + tmpPath, filePath.substr(1));
        }
        return path.join(tmpPath, filePath);
    });
}